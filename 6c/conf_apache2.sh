#Comprueba si apache esta instalado
echo "Comprobando si apache esta instalado.."
cmp=`which apache2`
if [ "$cmp" ]; then
        echo "Apache esta instalado"
else
        echo "Apache no esta isntalado.."
        apt-get install apache2
        echo "Instalado"
fi

#Cambiar el puesto de escucha por defecto al 90 REVISAR
echo "------------------------------"
echo "Modificando el puerto por defecto al 90 en los ficheros ports.conf y 000-default.conf"
#cd /etc/apache2/
#sed 's/Listen 80/Listen 90/' -i ports.conf
#cd /etc/apache2/sites-available/
#sed 's/80/90/' -i 000-default.conf
echo "------------------------------"

#Cambia la ruta por defecto a $HOME/bootstrap
echo "Se creara el directorio para la nueva ruta"
if [ -d "$HOME/bootstrap" ]; then
        echo "El directorio existe"
else
        echo "Directorio creado"
        mkdir -p  $HOME/bootstrap
fi
echo "Modificando ruta por defecto de apache2"
cd /etc/apache2/sites-available
home=$HOME
sed -i "s_DocumentRoot /var/www/html_DocumentRoot $home/bootstrap_" 000-default.conf
echo "La nueva ruta es: $HOME/bootstrap"
echo "------------------------------"

#Modificar fichero apache2.conf
echo "Modificando fichero apache2.conf para la nueva ruta"
cd /etc/apache2
sed -i 's_/var/www_/home_' apache2.conf
echo "------------------------------"

#Colocar la plantilla de bootstrap en la nueva ruta a traves de git.
echo "Se clonara la plantilla a traves de git"
read -p "Introduce la URL del repositorio de git: " repo
cd $HOME/bootstrap
git clone $repo .
echo "Repositorio clonado"
echo "Reiniciando apache"
/etc/init.d/apache2 restart
echo "Apache reiniciado"
