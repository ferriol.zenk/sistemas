#Programa que lee una columna de datos concreta y saca la media
#Corregido.
#Con un for no sería necesario la parte de head y tail, ni sacar el maximo con car y wc.
#Uso Calc para los calculos con decimales
maxl=`cat precipitaciones.txt | wc -l`
l=1
y=0

while [ $l -le $maxl ]; do

  n=`cat precipitaciones.txt | awk '{print $2}' | head -$l | tail -1`   
  y=`expr $n + $y`
  l=$(($l+1))

done

y=`calc $y/$maxl`
echo "La media de precipitaciones es de:$y"
