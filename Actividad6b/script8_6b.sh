#Detectar que ips estan conectadas en la red.
#Usare un ping y si da error con un grep sacare el +1 de la linea de errores.
#Corregido
#Mas enredada mi solucion, yo lo he basado en si da error, no hace nada, y si no hay error la IP es buena.
echo "Busqueda de Ips conectadas a la red local 192.168.8.0 \nprocesando.."

for i in `seq 2 254`; do
  error=`ping -t 2 -c 1 192.168.8.$i | grep +1`
 
  if [ "$error" ]; then
  echo "$error" >> /dev/null
  else
  echo "Ip 192.168.8.$i conectada" >> ip.txt 
  #echo "Ip 192.168.8.$i conectada"
  fi

done

for i in $(cat ip.txt); do
 echo "$i"
done

`rm -f ip.txt`

