#Script que lee un ficheros y muestra los num pare e impares y la cantidad de cada uno.
#Corregido.
#En vez del for para mostrar los ficheros usar un cat.
pares=0
impares=0

for i in $(cat numeros.txt); do  
 
  if [ `calc $i%2` -eq 0 ]; then
  echo "$i" >> pares.txt
  pares=$(($pares+1))
  else
  echo "$i" >> impares.txt
  impares=$(($impares+1)) 
  fi

done

echo "Numeros Pares"
for i in $(cat pares.txt); do
  echo "$i"
done
echo "----------------------"
echo "Cantidad de numeros pares total: $pares"

echo "Numeros impares"
for i in $(cat impares.txt);do
  echo "$i"
done
echo "----------------------"
echo "Cantidad de numeros impares total: $impares"
#Eliminamos los ficheros.
`rm -f pares.txt impares.txt`
