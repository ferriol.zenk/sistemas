#Lee un fichero, contabiliza los usuarios con Linux y Windows y suma el total de procesos de cada uno.
#Corregido.


maxl=`cat listado.txt | wc -l`
l=1
procLinux=0
soLinux=0
soWindows=0
procWindows=0

while [ $l -le $maxl ]; do

  so=`cat listado.txt | awk '{print $2}' | head -$l | tail -1`
  proc=`cat listado.txt | awk '{print $3}' | head -$l | tail -1`

  if [ $so = "Linux" ]; then
    soLinux=$(($soLinux+1))
    procLinux=$(($procLinux+$proc))
  else
    soWindows=$(($soWindows+1))
    procWindows=$(($procWindows+$proc))
  fi
l=$(($l+1))

done

echo "Linux: \nUso de SO Linux: $soLinux -- Procesos: $procLinux"
echo "Windows: \nUso de SO Winsdows: $soWindows -- Procesos: $procWindows"
