maxl=`cat usuarios.txt | wc -l`
l=1
suma=0
aux=0
coincidencia=0 #activador si hay una coincidencia
min=`cat usuarios.txt | awk '{print $3}' | head -$l | tail -1` #cargo el primer valor para la comparacion.

if [ "$1" ] && [ $# -eq 1 ]; then #con $# nos devuelve el num de parametros
    while [ $l -le $maxl ]; do
    #nombre 
    n=`cat usuarios.txt | awk '{print $2}' | head -$l | tail -1`  
    #cantidad
    c=`cat usuarios.txt | awk '{print $3}' | head -$l | tail -1`

       if [ `echo $n | grep ^$1` ]; then
       coincidencia=1 
       echo "$n" >> resultado.txt
       suma=$(($suma+$c)) #sumo el numero de accesos
          if [ $c -lt $min ]; then # comparacion para sacar el min
          min=$c
          user=$n
          fi
       fi
   l=$(($l+1))
   done
   if [ $suma -gt 0 ]; then #Comprobación , si ha incrementado ese contador, añadira las lineas al fichero
   echo "La cantidad total de accesos con usuarios con ese patron ($1) es: $suma" >> resultado.txt
   echo "El login con menos accesos ($min) con ese patron es: $user" >> resultado.txt
   fi
   if [ $coincidencia -eq 0 ]; then #Si el activador está a 0 no ha habiado coincidencias y muestra el mensaje
   echo "No ha habido coincidencias"
   fi
else echo "No has pasado un parametro al script o has pasado mas de uno"
fi

##Entiendo que se pretende guardar un registro, así que esto es mas que nada por comodida para comprobar que funciona el script.
if [ -f resultado.txt ]; then
echo "`cat resultado.txt`"
rm resultado.txt
fi
