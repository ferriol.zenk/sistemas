## pedir un valor mayor que 0 e indicar si es par o impar. 
## Verificar si el valor introducido es correcto, si no lo es
## volver a pedirlo.

## Corregido

salida=0

while [ $salida -eq 0 ]; do
	read -p "Introduce un valor: " valor
	 if [ $valor -le 0 ]; then
 	  echo "Valor incorrecto"
	 else
	  salida=1
	 fi
done

if [ `expr $valor % 2` -eq 0 ] ;then
 echo "El valor $valor es par"
else
 echo "El valor $valor es impar"
fi



















