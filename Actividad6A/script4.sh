# Script que pida un valor mayor que 0 y muestre el listad
# desde 0 a ese valor.
#Corregido.

#Comprobacion del valor introducido
comprobacion=0

while [ $comprobacion -eq 0 ]; do
	read -p "Introduce un valor mayor que 0: " valor
	if [ $valor -gt 0 ]; then
	comprobacion=1
	else
	echo "Valor incorrecto"
	fi
done

#Funcionamiento
if [ $comprobacion -eq 1 ]; then
	for i in `seq 0 $valor`; do
	echo "$i"
	done
fi
