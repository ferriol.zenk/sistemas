#Pide numeros hasta que se introduzca un 0 , y sume los valores introducidos
#Corregido. Lei mal el enunciado, me faltaba calcular la media.
salida=0
contador=0
while [ $salida -eq 0 ];do
	
	read -p "Introduce un valor (Para salir introduce 0):" valor
	sumandos=$sumandos/$valor
	resultado=`expr $resultado + $valor`
	contador=$(($contador+1))
	if [ $valor -eq 0 ]; then salida=1
	fi	
done
media=`echo "scale=2; $resultado/$contador" | bc`
echo "Numros a calcular: $sumandos"
echo "Resultado: $resultado"
echo "La media es: $media"
