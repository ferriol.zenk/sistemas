#Corregido.
#La diferenencia es que lo he hecho con calc. 
#Sobran las condiciones -gt de los if.


#Comprobacion del valor introducido
comprobacion=0

while [ $comprobacion -eq 0 ]; do
        read -p "Introduce los litros consumidos: " valor
        if [ $valor -gt 0 ]; then
        comprobacion=1
        else
        echo "Valor incorrecto"
        fi
done

#Funcionamiento
#Instalar calc con sudo apt-get instal apcalc. Este programa permite calculos con decimales

coste=0

if [ $comprobacion -eq 1 ]; then

  if [ $valor -le 50 ]; then coste=20
  else

    if [ $valor -gt 50 ] && [ $valor -le 200 ]; then 
    restoLitros=`expr $valor - 50`
    coste=`calc 20+$restoLitros*0.20`
    else

      if [ $valor -gt 200 ]; then 
      restoLitros=`expr $valor - 200`
      coste=`calc 50+$restoLitros*0.10`
      fi  
    fi
  fi  
fi

echo "El coste final es de: $coste"
